HouseholdSim
============

This is pseudo simulator to serve residual load profiles to mosaik.

Status
------

.. image:: https://gitlab.com/offis.energy/mosaik/mosaik.householdsim_semver/badges/master/pipeline.svg
    :target: https://gitlab.com/offis.energy/mosaik/mosaik.householdsim_semver/-/jobs
    :alt: Pipeline status

.. image:: https://gitlab.com/offis.energy/mosaik/mosaik.householdsim_semver/badges/master/coverage.svg
    :target: https://gitlab.com/offis.energy/mosaik/mosaik.householdsim_semver/commits/master
    :alt: Coverage Report

.. image:: https://img.shields.io/librariesio/release/pypi/mosaik.householdsim_semver
    :target: https://libraries.io/pypi/mosaik.householdsim_semver
    :alt: Libraries status

.. image:: https://img.shields.io/pypi/l/mosaik.householdsim_semver
    :target: https://pypi.org/project/mosaik.householdsim_semver/
    :alt: License badge

.. image:: https://img.shields.io/pypi/v/mosaik.householdsim_semver
    :target: https://pypi.org/project/mosaik.householdsim_semver/#history
    :alt: PyPI version

.. image:: https://img.shields.io/pypi/pyversions/mosaik.householdsim_semver
    :target: https://pypi.org/project/mosaik.householdsim_semver/
    :alt: Python Versions

Installation
------------

::

    $ pip install mosaik-householdsim

Tests
-----

You can run the tests with::

    $ git clone https://bitbucket.org/mosaik/mosaik-householdsim
    $ cd mosaik-householdsim
    $ pip install -r requirements.txt
    $ pip install -e .
    $ pytest
    $ tox


Documentation
-------------

This simulator consists of a a *model* (``householdsim/model.py``) and the
mosaik API implemenation (``householdsim/mosaik.py``).

The model processes the data from a NumPy *\*.npz* file (see
``householdsim/test/test_model.py`` for an example of its layout). Basically,
the file contains a number of load profiles for a given period of time. It
also contains *ID lists* that describe which load profile belongs to which
node ID in a power grid. The first entry in an ID list relates to the first
entry of the profiles list, the second entry in the ID list to the second
load profile and so on. If the number of entires in the ID list is larger than
the number of load profiles, we start again with the first load profile.

Internally, the model works with minute. Since mosaik is based on seconds,
the mosaik API implementation converts between them.

Usually, residual load profiles have a resolution of 15 minutes. It is no
problem for this simulator to step in 1 minute steps, though.
